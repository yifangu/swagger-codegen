"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TasksApi = void 0;

var _ApiClient = require("../ApiClient");

var _V2TasksBody = require("../model/V2TasksBody");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
* Tasks service.
* @module api/TasksApi
* @version 1.0.0
*/
var TasksApi = /*#__PURE__*/function () {
  /**
  * Constructs a new TasksApi. 
  * @alias module:api/TasksApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instanc
  e} if unspecified.
  */
  function TasksApi(apiClient) {
    _classCallCheck(this, TasksApi);

    this.apiClient = apiClient || _ApiClient.ApiClient.instance;
  }
  /**
   * Callback function to receive the result of the createTask operation.
   * @callback moduleapi/TasksApi~createTaskCallback
   * @param {String} error Error message, if any.
   * @param data This operation does not return a value.
   * @param {String} response The complete HTTP response.
   */

  /**
   * Create a task
   * Creates a new task for the current tenant.  Tasks are created in the \&quot;outstanding\&quot; state. 
   * @param {module:model/V2TasksBody} body 
   * @param {module:api/TasksApi~createTaskCallback} callback The callback function, accepting three arguments: error, data, response
   */


  _createClass(TasksApi, [{
    key: "createTask",
    value: function createTask(body, callback) {
      var postBody = body; // verify the required parameter 'body' is set

      if (body === undefined || body === null) {
        throw new Error("Missing the required parameter 'body' when calling createTask");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['role'];
      var contentTypes = ['application/json'];
      var accepts = [];
      var returnType = null;
      return this.apiClient.callApi('/v2/tasks', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, callback);
    }
    /**
     * Callback function to receive the result of the deleteTask operation.
     * @callback moduleapi/TasksApi~deleteTaskCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Delete a task
     * Deletes the specified task
     * @param {String} id ID of the task
     * @param {module:api/TasksApi~deleteTaskCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "deleteTask",
    value: function deleteTask(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling deleteTask");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['role'];
      var contentTypes = [];
      var accepts = [];
      var returnType = null;
      return this.apiClient.callApi('/v2/tasks/:id', 'DELETE', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, callback);
    }
    /**
     * Callback function to receive the result of the getTasks operation.
     * @callback moduleapi/TasksApi~getTasksCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * List tasks
     * Returns the collection of all tasks for the current customer.
     * @param {Object} opts Optional parameters
     * @param {String} opts.associated_id ID of the object associated with the task to filter by
     * @param {module:model/String} opts.associated_type Type of the associated object to filter by
     * @param {module:model/String} opts.status Filter for the current status of the task
     * @param {String} opts.user_id Filter for the creator of the task
     * @param {Array.<String>} opts.assigned_to Filter for the assignee of the task
     * @param {String} opts.sort Sort order of the response.  Sorts are provided in the form of &#x60;[ATTRIBUTE]_[DIR]&#x60; where &#x60;DIR&#x60; is either &#x60;asc&#x60; or &#x60;desc&#x60;.  Multiple sorts can be achieved by joining the sorts by a comma, i.e. &#x60;sort&#x3D;status_desc,created_at_asc&#x60;.  The attributes that are supported for sorting are:   - created_at   - due_at   - completed_at   - status   - user_id 
     * @param {Number} opts.page_size For pagination, the number of records to return.
     * @param {Number} opts.page_offset For pagination, the number of records to skip.
     * @param {module:api/TasksApi~getTasksCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "getTasks",
    value: function getTasks(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'associated_id': opts['associated_id'],
        'associated_type': opts['associated_type'],
        'status': opts['status'],
        'user_id': opts['user_id'],
        'assigned_to': this.apiClient.buildCollectionParam(opts['assigned_to'], 'multi'),
        'sort': opts['sort'],
        'page_size': opts['page_size'],
        'page_offset': opts['page_offset']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['role'];
      var contentTypes = [];
      var accepts = [];
      var returnType = null;
      return this.apiClient.callApi('/v2/tasks', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, callback);
    }
    /**
     * Callback function to receive the result of the updateTask operation.
     * @callback moduleapi/TasksApi~updateTaskCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Update a Task
     * Updates the specified task attributes
     * @param {String} id ID of the task
     * @param {module:api/TasksApi~updateTaskCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "updateTask",
    value: function updateTask(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling updateTask");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['role'];
      var contentTypes = [];
      var accepts = [];
      var returnType = null;
      return this.apiClient.callApi('/v2/tasks/:id', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, callback);
    }
  }]);

  return TasksApi;
}();

exports.TasksApi = TasksApi;