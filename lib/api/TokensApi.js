"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TokensApi = void 0;

var _ApiClient = require("../ApiClient");

var _UserTokenBody = require("../model/UserTokenBody");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
* Tokens service.
* @module api/TokensApi
* @version 1.0.0
*/
var TokensApi = /*#__PURE__*/function () {
  /**
  * Constructs a new TokensApi. 
  * @alias module:api/TokensApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instanc
  e} if unspecified.
  */
  function TokensApi(apiClient) {
    _classCallCheck(this, TokensApi);

    this.apiClient = apiClient || _ApiClient.ApiClient.instance;
  }
  /**
   * Callback function to receive the result of the createUserToken operation.
   * @callback moduleapi/TokensApi~createUserTokenCallback
   * @param {String} error Error message, if any.
   * @param data This operation does not return a value.
   * @param {String} response The complete HTTP response.
   */

  /**
   * Obtain Access Token
   * creates an access token given valid credentials
   * @param {module:model/UserTokenBody} body 
   * @param {module:api/TokensApi~createUserTokenCallback} callback The callback function, accepting three arguments: error, data, response
   */


  _createClass(TokensApi, [{
    key: "createUserToken",
    value: function createUserToken(body, callback) {
      var postBody = body; // verify the required parameter 'body' is set

      if (body === undefined || body === null) {
        throw new Error("Missing the required parameter 'body' when calling createUserToken");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = [];
      var returnType = null;
      return this.apiClient.callApi('/user_token', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, callback);
    }
  }]);

  return TokensApi;
}();

exports.TokensApi = TokensApi;