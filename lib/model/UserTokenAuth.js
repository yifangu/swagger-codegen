"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UserTokenAuth = void 0;

var _ApiClient = require("../ApiClient");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The UserTokenAuth model module.
 * @module model/UserTokenAuth
 * @version 1.0.0
 */
var UserTokenAuth = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>UserTokenAuth</code>.
   * @alias module:model/UserTokenAuth
   * @class
   * @param email {String} 
   * @param password {String} 
   */
  function UserTokenAuth(email, password) {
    _classCallCheck(this, UserTokenAuth);

    this.email = email;
    this.password = password;
  }
  /**
   * Constructs a <code>UserTokenAuth</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/UserTokenAuth} obj Optional instance to populate.
   * @return {module:model/UserTokenAuth} The populated <code>UserTokenAuth</code> instance.
   */


  _createClass(UserTokenAuth, null, [{
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new UserTokenAuth();
        if (data.hasOwnProperty('email')) obj.email = _ApiClient.ApiClient.convertToType(data['email'], 'String');
        if (data.hasOwnProperty('password')) obj.password = _ApiClient.ApiClient.convertToType(data['password'], 'String');
      }

      return obj;
    }
  }]);

  return UserTokenAuth;
}();
/**
 * @member {String} email
 */


exports.UserTokenAuth = UserTokenAuth;
UserTokenAuth.prototype.email = undefined;
/**
 * @member {String} password
 */

UserTokenAuth.prototype.password = undefined;