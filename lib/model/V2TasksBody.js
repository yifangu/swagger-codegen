"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.V2TasksBody = void 0;

var _ApiClient = require("../ApiClient");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The V2TasksBody model module.
 * @module model/V2TasksBody
 * @version 1.0.0
 */
var V2TasksBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>V2TasksBody</code>.
   * @alias module:model/V2TasksBody
   * @class
   * @param id {String} 
   * @param subject {String} 
   */
  function V2TasksBody(id, subject) {
    _classCallCheck(this, V2TasksBody);

    this.id = id;
    this.subject = subject;
  }
  /**
   * Constructs a <code>V2TasksBody</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/V2TasksBody} obj Optional instance to populate.
   * @return {module:model/V2TasksBody} The populated <code>V2TasksBody</code> instance.
   */


  _createClass(V2TasksBody, null, [{
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new V2TasksBody();
        if (data.hasOwnProperty('id')) obj.id = _ApiClient.ApiClient.convertToType(data['id'], 'String');
        if (data.hasOwnProperty('subject')) obj.subject = _ApiClient.ApiClient.convertToType(data['subject'], 'String');
        if (data.hasOwnProperty('notes')) obj.notes = _ApiClient.ApiClient.convertToType(data['notes'], 'String');
        if (data.hasOwnProperty('due_at')) obj.due_at = _ApiClient.ApiClient.convertToType(data['due_at'], 'Date');
        if (data.hasOwnProperty('associated_id')) obj.associated_id = _ApiClient.ApiClient.convertToType(data['associated_id'], 'String');
        if (data.hasOwnProperty('associated_type')) obj.associated_type = _ApiClient.ApiClient.convertToType(data['associated_type'], 'String');
        if (data.hasOwnProperty('assignee_ids')) obj.assignee_ids = _ApiClient.ApiClient.convertToType(data['assignee_ids'], ['String']);
      }

      return obj;
    }
  }]);

  return V2TasksBody;
}();
/**
 * @member {String} id
 */


exports.V2TasksBody = V2TasksBody;
V2TasksBody.prototype.id = undefined;
/**
 * @member {String} subject
 */

V2TasksBody.prototype.subject = undefined;
/**
 * @member {String} notes
 */

V2TasksBody.prototype.notes = undefined;
/**
 * ISO-8601 formatted due date
 * @member {Date} due_at
 */

V2TasksBody.prototype.due_at = undefined;
/**
 * ID of the associated entity
 * @member {String} associated_id
 */

V2TasksBody.prototype.associated_id = undefined;
/**
 * Allowed values for the <code>associated_type</code> property.
 * @enum {String}
 * @readonly
 */

V2TasksBody.AssociatedTypeEnum = {
  /**
   * value: "person"
   * @const
   */
  person: "person",

  /**
   * value: "organization"
   * @const
   */
  organization: "organization",

  /**
   * value: "activity"
   * @const
   */
  activity: "activity"
};
/**
 * Type of the associated entity
 * @member {module:model/V2TasksBody.AssociatedTypeEnum} associated_type
 */

V2TasksBody.prototype.associated_type = undefined;
/**
 * IDs of users to assign this task to
 * @member {Array.<String>} assignee_ids
 */

V2TasksBody.prototype.assignee_ids = undefined;