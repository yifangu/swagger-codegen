"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UserTokenBody = void 0;

var _ApiClient = require("../ApiClient");

var _UserTokenAuth = require("./UserTokenAuth");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The UserTokenBody model module.
 * @module model/UserTokenBody
 * @version 1.0.0
 */
var UserTokenBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>UserTokenBody</code>.
   * @alias module:model/UserTokenBody
   * @class
   * @param auth {module:model/UserTokenAuth} 
   */
  function UserTokenBody(auth) {
    _classCallCheck(this, UserTokenBody);

    this.auth = auth;
  }
  /**
   * Constructs a <code>UserTokenBody</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/UserTokenBody} obj Optional instance to populate.
   * @return {module:model/UserTokenBody} The populated <code>UserTokenBody</code> instance.
   */


  _createClass(UserTokenBody, null, [{
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new UserTokenBody();
        if (data.hasOwnProperty('auth')) obj.auth = _UserTokenAuth.UserTokenAuth.constructFromObject(data['auth']);
      }

      return obj;
    }
  }]);

  return UserTokenBody;
}();
/**
 * @member {module:model/UserTokenAuth} auth
 */


exports.UserTokenBody = UserTokenBody;
UserTokenBody.prototype.auth = undefined;