# irwin_api

IrwinApi - JavaScript client for irwin_api
Documentation for the Irwin API  # Authentication  <!-- ReDoc-Inject: <security-definitions> -->
This SDK is automatically generated by the [Swagger Codegen](https://github.com/swagger-api/swagger-codegen) project:

- API version: 1.0.0
- Package version: 1.0.0
- Build package: io.swagger.codegen.v3.generators.javascript.JavaScriptClientCodegen

## Installation

### For [Node.js](https://nodejs.org/)

#### npm

To publish the library as a [npm](https://www.npmjs.com/),
please follow the procedure in ["Publishing npm packages"](https://docs.npmjs.com/getting-started/publishing-npm-packages).

Then install it via:

```shell
npm install irwin_api --save
```

#### git
#
If the library is hosted at a git repository, e.g.
https://github.com/GIT_USER_ID/GIT_REPO_ID
then install it via:

```shell
    npm install GIT_USER_ID/GIT_REPO_ID --save
```

### For browser

The library also works in the browser environment via npm and [browserify](http://browserify.org/). After following
the above steps with Node.js and installing browserify with `npm install -g browserify`,
perform the following (assuming *main.js* is your entry file):

```shell
browserify main.js > bundle.js
```

Then include *bundle.js* in the HTML pages.

### Webpack Configuration

Using Webpack you may encounter the following error: "Module not found: Error:
Cannot resolve module", most certainly you should disable AMD loader. Add/merge
the following section to your webpack config:

```javascript
module: {
  rules: [
    {
      parser: {
        amd: false
      }
    }
  ]
}
```

## Getting Started

Please follow the [installation](#installation) instruction and execute the following JS code:

```javascript
var IrwinApi = require('irwin_api');
var defaultClient = IrwinApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: role
var role = defaultClient.authentications['role'];
role.accessToken = "YOUR ACCESS TOKEN"

var api = new IrwinApi.TasksApi()
var body = new IrwinApi.V2TasksBody(); // {V2TasksBody} 

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
api.createTask(body, callback);
```

## Documentation for API Endpoints

All URIs are relative to *https://api.staging.imirwin.com*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*IrwinApi.TasksApi* | [**createTask**](docs/TasksApi.md#createTask) | **POST** /v2/tasks | Create a task
*IrwinApi.TasksApi* | [**deleteTask**](docs/TasksApi.md#deleteTask) | **DELETE** /v2/tasks/:id | Delete a task
*IrwinApi.TasksApi* | [**getTasks**](docs/TasksApi.md#getTasks) | **GET** /v2/tasks | List tasks
*IrwinApi.TasksApi* | [**updateTask**](docs/TasksApi.md#updateTask) | **PUT** /v2/tasks/:id | Update a Task
*IrwinApi.TokensApi* | [**createUserToken**](docs/TokensApi.md#createUserToken) | **POST** /user_token | Obtain Access Token

## Documentation for Models

 - [IrwinApi.UserTokenAuth](docs/UserTokenAuth.md)
 - [IrwinApi.UserTokenBody](docs/UserTokenBody.md)
 - [IrwinApi.V2TasksBody](docs/V2TasksBody.md)

## Documentation for Authorization


### role

- **Type**: OAuth
- **Flow**: implicit
- **Authorization URL**: n/a
- **Scopes**: 
  - : 

