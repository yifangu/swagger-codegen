/*
 * Irwin API
 * Documentation for the Irwin API  # Authentication  <!-- ReDoc-Inject: <security-definitions> -->
 *
 * OpenAPI spec version: 1.0.0
 * Contact: will@getirwin.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 3.0.26
 *
 * Do not edit the class manually.
 *
 */
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.IrwinApi);
  }
}(this, function(expect, IrwinApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new IrwinApi.TasksApi();
  });

  describe('(package)', function() {
    describe('TasksApi', function() {
      describe('createTask', function() {
        it('should call createTask successfully', function(done) {
          // TODO: uncomment, update parameter values for createTask call
          /*

          instance.createTask(body, function(error, data, response) {
            if (error) {
              done(error);
              return;
            }

            done();
          });
          */
          // TODO: uncomment and complete method invocation above, then delete this line and the next:
          done();
        });
      });
      describe('deleteTask', function() {
        it('should call deleteTask successfully', function(done) {
          // TODO: uncomment, update parameter values for deleteTask call
          /*

          instance.deleteTask(id, function(error, data, response) {
            if (error) {
              done(error);
              return;
            }

            done();
          });
          */
          // TODO: uncomment and complete method invocation above, then delete this line and the next:
          done();
        });
      });
      describe('getTasks', function() {
        it('should call getTasks successfully', function(done) {
          // TODO: uncomment, update parameter values for getTasks call
          /*
          var opts = {};

          instance.getTasks(opts, function(error, data, response) {
            if (error) {
              done(error);
              return;
            }

            done();
          });
          */
          // TODO: uncomment and complete method invocation above, then delete this line and the next:
          done();
        });
      });
      describe('updateTask', function() {
        it('should call updateTask successfully', function(done) {
          // TODO: uncomment, update parameter values for updateTask call
          /*

          instance.updateTask(id, function(error, data, response) {
            if (error) {
              done(error);
              return;
            }

            done();
          });
          */
          // TODO: uncomment and complete method invocation above, then delete this line and the next:
          done();
        });
      });
    });
  });

}));
