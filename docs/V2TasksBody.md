# IrwinApi.V2TasksBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**subject** | **String** |  | 
**notes** | **String** |  | [optional] 
**due_at** | **Date** | ISO-8601 formatted due date | [optional] 
**associated_id** | **String** | ID of the associated entity | [optional] 
**associated_type** | **String** | Type of the associated entity | [optional] 
**assignee_ids** | **[String]** | IDs of users to assign this task to | [optional] 

<a name="AssociatedTypeEnum"></a>
## Enum: AssociatedTypeEnum

* `person` (value: `"person"`)
* `organization` (value: `"organization"`)
* `activity` (value: `"activity"`)

