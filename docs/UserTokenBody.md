# IrwinApi.UserTokenBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auth** | [**UserTokenAuth**](UserTokenAuth.md) |  | 
