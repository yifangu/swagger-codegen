# IrwinApi.TokensApi

All URIs are relative to *https://api.staging.imirwin.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createUserToken**](TokensApi.md#createUserToken) | **POST** /user_token | Obtain Access Token

<a name="createUserToken"></a>
# **createUserToken**
> createUserToken(body)

Obtain Access Token

creates an access token given valid credentials

### Example
```javascript
import {IrwinApi} from 'irwin_api';

let apiInstance = new IrwinApi.TokensApi();
let body = new IrwinApi.UserTokenBody(); // UserTokenBody | 

apiInstance.createUserToken(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**UserTokenBody**](UserTokenBody.md)|  | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

