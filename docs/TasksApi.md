# IrwinApi.TasksApi

All URIs are relative to *https://api.staging.imirwin.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createTask**](TasksApi.md#createTask) | **POST** /v2/tasks | Create a task
[**deleteTask**](TasksApi.md#deleteTask) | **DELETE** /v2/tasks/:id | Delete a task
[**getTasks**](TasksApi.md#getTasks) | **GET** /v2/tasks | List tasks
[**updateTask**](TasksApi.md#updateTask) | **PUT** /v2/tasks/:id | Update a Task

<a name="createTask"></a>
# **createTask**
> createTask(body)

Create a task

Creates a new task for the current tenant.  Tasks are created in the \&quot;outstanding\&quot; state. 

### Example
```javascript
import {IrwinApi} from 'irwin_api';
let defaultClient = IrwinApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: role
let role = defaultClient.authentications['role'];
role.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new IrwinApi.TasksApi();
let body = new IrwinApi.V2TasksBody(); // V2TasksBody | 

apiInstance.createTask(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**V2TasksBody**](V2TasksBody.md)|  | 

### Return type

null (empty response body)

### Authorization

[role](../README.md#role)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="deleteTask"></a>
# **deleteTask**
> deleteTask(id)

Delete a task

Deletes the specified task

### Example
```javascript
import {IrwinApi} from 'irwin_api';
let defaultClient = IrwinApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: role
let role = defaultClient.authentications['role'];
role.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new IrwinApi.TasksApi();
let id = "38400000-8cf0-11bd-b23e-10b96e4ef00d"; // String | ID of the task

apiInstance.deleteTask(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**String**](.md)| ID of the task | 

### Return type

null (empty response body)

### Authorization

[role](../README.md#role)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getTasks"></a>
# **getTasks**
> getTasks(opts)

List tasks

Returns the collection of all tasks for the current customer.

### Example
```javascript
import {IrwinApi} from 'irwin_api';
let defaultClient = IrwinApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: role
let role = defaultClient.authentications['role'];
role.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new IrwinApi.TasksApi();
let opts = { 
  'associated_id': "38400000-8cf0-11bd-b23e-10b96e4ef00d", // String | ID of the object associated with the task to filter by
  'associated_type': "associated_type_example", // String | Type of the associated object to filter by
  'status': "status_example", // String | Filter for the current status of the task
  'user_id': "38400000-8cf0-11bd-b23e-10b96e4ef00d", // String | Filter for the creator of the task
  'assigned_to': ["assigned_to_example"], // [String] | Filter for the assignee of the task
  'sort': "sort_example", // String | Sort order of the response.  Sorts are provided in the form of `[ATTRIBUTE]_[DIR]` where `DIR` is either `asc` or `desc`.  Multiple sorts can be achieved by joining the sorts by a comma, i.e. `sort=status_desc,created_at_asc`.  The attributes that are supported for sorting are:   - created_at   - due_at   - completed_at   - status   - user_id 
  'page_size': 56, // Number | For pagination, the number of records to return.
  'page_offset': 56 // Number | For pagination, the number of records to skip.
};
apiInstance.getTasks(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **associated_id** | [**String**](.md)| ID of the object associated with the task to filter by | [optional] 
 **associated_type** | **String**| Type of the associated object to filter by | [optional] 
 **status** | **String**| Filter for the current status of the task | [optional] 
 **user_id** | [**String**](.md)| Filter for the creator of the task | [optional] 
 **assigned_to** | [**[String]**](String.md)| Filter for the assignee of the task | [optional] 
 **sort** | **String**| Sort order of the response.  Sorts are provided in the form of &#x60;[ATTRIBUTE]_[DIR]&#x60; where &#x60;DIR&#x60; is either &#x60;asc&#x60; or &#x60;desc&#x60;.  Multiple sorts can be achieved by joining the sorts by a comma, i.e. &#x60;sort&#x3D;status_desc,created_at_asc&#x60;.  The attributes that are supported for sorting are:   - created_at   - due_at   - completed_at   - status   - user_id  | [optional] 
 **page_size** | **Number**| For pagination, the number of records to return. | [optional] 
 **page_offset** | **Number**| For pagination, the number of records to skip. | [optional] 

### Return type

null (empty response body)

### Authorization

[role](../README.md#role)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="updateTask"></a>
# **updateTask**
> updateTask(id)

Update a Task

Updates the specified task attributes

### Example
```javascript
import {IrwinApi} from 'irwin_api';
let defaultClient = IrwinApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: role
let role = defaultClient.authentications['role'];
role.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new IrwinApi.TasksApi();
let id = "38400000-8cf0-11bd-b23e-10b96e4ef00d"; // String | ID of the task

apiInstance.updateTask(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**String**](.md)| ID of the task | 

### Return type

null (empty response body)

### Authorization

[role](../README.md#role)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

